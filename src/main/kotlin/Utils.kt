import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

fun logTime(action: suspend CoroutineScope.() -> Unit) {
    println("Started")
    val time = measureTimeMillis {
        runBlocking {
            action()
        }
    }
    println("\nFinished in $time ms")
}

fun newLineAfterEach100(i: Int) {
    if (i.plus(1).rem(100) == 0) {
        println()
    }
}