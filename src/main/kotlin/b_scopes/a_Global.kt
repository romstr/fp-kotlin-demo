package b_scopes

import kotlinx.coroutines.*

fun main() {

    GlobalScope.launch {
        delay(1000L)
        println("coroutines!")
    }
    print("Hello, ")
    Thread.sleep(2000L)

}