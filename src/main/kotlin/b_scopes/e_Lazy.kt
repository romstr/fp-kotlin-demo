package b_scopes

import kotlinx.coroutines.*
import logTime

fun main() = logTime {

    val result = async(start = CoroutineStart.LAZY) {
        delay(1000L)
        println(":) ")
        "coroutines!"
    }
    delay(1000L)
    print("Hello, ${result.await()}")

}