package b_scopes

import kotlinx.coroutines.*
import logTime

fun main() = logTime {

    val result = async {
        delay(1000L)
        "coroutines!"
    }
    delay(1000L)
    print("Hello, ${result.await()}")

}