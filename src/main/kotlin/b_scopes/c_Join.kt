package b_scopes

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {

    val job = launch {
        delay(1000L)
        println("coroutines!")
    }
    job.join()
    print("Hello, ")

}