package a_comparison

import kotlinx.coroutines.*
import logTime
import newLineAfterEach100

fun main() = logTime {
    repeat(100_000) { i ->
        launch {
            delay(1000L)
            print(".")
            newLineAfterEach100(i)
        }
    }
}
