package a_comparison

import logTime
import newLineAfterEach100
import kotlin.concurrent.thread

fun main() = logTime {
    repeat(100_000) { i ->
        thread {
            Thread.sleep(1000L)
            print(".")
            newLineAfterEach100(i)
        }
    }
}
